/*
    This file is part of Element
    Copyright (C) 2019  Kushview, LLC.  All rights reserved.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#pragma once

#include "nodes/baseprocessor.hpp"

namespace element {

class AudioSwitchProcessor  : public BaseProcessor
{
public:
    enum BusSelectedEnum {
        outA = 0, 
        outB = 1,
        outC = 2,
        outD = 3,
        outMax = 4,
    };

private:
    int  numChanInputs  = 2;
    int  numChanOutputs = 8;
    bool acceptMidi  = false;
    bool produceMidi = false;
	int  currentOutputBus = outA;

    AudioParameterBool* selectedBus { nullptr };


public:
    AudioSwitchProcessor ();

    ~AudioSwitchProcessor ();

    inline const String getName() const override { return "Audio Switch"; }

    inline void fillInPluginDescription (PluginDescription& d) const override
    {
        d.name = getName();
        d.descriptiveName = "Audio Switch";
        d.version = "1.1.0";
        d.pluginFormatName = "Element";
        d.manufacturerName = "Element";
        d.fileOrIdentifier = EL_NODE_ID_AUDIO_SWITCH;
        d.numInputChannels = numChanInputs;
        d.numOutputChannels = numChanOutputs;
        d.hasSharedContainer = false;
        d.isInstrument = false;
    }

    void prepareToPlay (double sampleRate, int maximumExpectedSamplesPerBlock);

    inline void releaseResources() override {}


    void processBlock (AudioBuffer<float>& buffer, MidiBuffer&);

    inline double getTailLengthSeconds() const override { return 0.0; }

    inline bool acceptsMidi() const override { return acceptMidi; }
    inline bool producesMidi() const override { return produceMidi; }

    AudioProcessorEditor* createEditor();
    bool hasEditor() const override { return true; }

    void getStateInformation (juce::MemoryBlock& destData) override;

    void setStateInformation (const void* data, int sizeInBytes) override;

    inline int getNumPrograms() override { return 1; };
    inline int getCurrentProgram() override { return 1; };
    inline void setCurrentProgram (int index) override { ignoreUnused (index); };
    inline const String getProgramName (int index) override
    {
        ignoreUnused (index);
        return "Default";
    }
    inline void changeProgramName (int index, const String& newName) override { ignoreUnused (index, newName); }

    void numBusesChanged(); 

#if 0
    // Audio Processor Template
    virtual StringArray getAlternateDisplayNames() const;
    
    virtual void processBlock (AudioBuffer<double>& buffer, idiBuffer& midiMessages);
    
    virtual void processBlockBypassed (AudioBuffer<float>& buffer, MidiBuffer& midiMessages);
    virtual void processBlockBypassed (AudioBuffer<double>& buffer, MidiBuffer& midiMessages);
    virtual bool canAddBus (bool isInput) const                     { ignoreUnused (isInput); return false; }
    virtual bool canRemoveBus (bool isInput) const                  { ignoreUnused (isInput); return false; }
    virtual bool supportsDoublePrecisionProcessing() const;
    virtual bool supportsMPE() const                            { return false; }
    virtual bool isMidiEffect() const                           { return false; }
    virtual void reset();
    virtual void setNonRealtime (bool isNonRealtime) noexcept;
    
    virtual void getCurrentProgramStateInformation (juce::MemoryBlock& destData);
    virtual void setCurrentProgramStateInformation (const void* data, int sizeInBytes);
    virtual void numChannelsChanged();
    virtual void numBusesChanged();
    virtual void processorLayoutsChanged();
    virtual void addListener (AudioProcessorListener* newListener);
    virtual void removeListener (AudioProcessorListener* listenerToRemove);
    virtual void setPlayHead (AudioPlayHead* newPlayHead);
    virtual void updateTrackProperties (const TrackProperties& properties);
    virtual void fillInPluginDescription (PluginDescription& description);

protected:

#endif

    class Listener
    {
    public:
        virtual ~Listener() {}
        virtual void updateAudioSwitch (int /*audioSwitch*/) {}
    };

    void addAudioSwitchListener (Listener* l) { listeners.add (l); }
    void removeAudioSwitchListener (Listener* l) { listeners.remove (l); }

    bool canAddBus    (bool isInput) const;
    bool canRemoveBus (bool isInput) const;
    
protected:
    bool isBusesLayoutSupported (const BusesLayout& layout) const override;

    bool canApplyBusesLayout (const BusesLayout& layouts) const override { return isBusesLayoutSupported (layouts); }

    bool canApplyBusCountChange (bool isInput, bool isAdding, AudioProcessor::BusProperties& outProperties) override;

    int getOutputBusesNumber() const
    { 
        return numChanOutputs/2; //all buses are stereo, so 4 chan outputs means 2 buses (left/right)!!
    }

    // this is called from the editor to know which is the current selected output bus
    int getSelectedOutputBus() const
    { 
        return currentOutputBus;
    }

    // this is called from the editor when the user selects a new output bus
    void setSelectedOutputBus(const int newOutBus) 
    { 
        currentOutputBus = newOutBus;
        listeners.call (&Listener::updateAudioSwitch, currentOutputBus);
    }

private:
    friend class AudioSwitchEditor;
    ListenerList<Listener> listeners;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (AudioSwitchProcessor)
};

} // namespace element
