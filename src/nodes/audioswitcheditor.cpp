/*
    This file is part of Element
    Copyright (C) 2019  Kushview, LLC.  All rights reserved.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "nodes/audioswitch.h"
#include "nodes/audioswitcheditor.h"
#include <element/ui/style.hpp>
#include "common.hpp"

namespace element {

class MyTextButton :  public TextButton 
{
public:
    int instance;
    void setInstance(int inst)
    {
        instance = inst;
    }
    int getInstance()
    {
        return instance;
    }
};

// ================================================================================================
// AudioSwitchEditor::Content

class AudioSwitchEditor::Content :  public Component, 
                                    private AudioSwitchProcessor::Listener
{
public:
    Content (AudioSwitchEditor& _editor) 
    {
        setOpaque (true);

        proc = _editor.proc;

        numOutBuses = proc->getOutputBusesNumber();
        busSelected = proc->getSelectedOutputBus();

        addAndMakeVisible (busSelectLabel);
        busSelectLabel.setColour (juce::Label::backgroundColourId, juce::Colours::black);
        busSelectLabel.setColour (juce::Label::textColourId, juce::Colours::white);
        busSelectLabel.setJustificationType (juce::Justification::centred);
        busSelectLabel.setText ("Select the desired bus output:", dontSendNotification);

        for (int out = 0; out < numOutBuses; out++) {
            addAndMakeVisible (OutputButton[out]);
            OutputButton[out].setToggleable(true);
            OutputButton[out].setColour (juce::TextButton::buttonColourId ,  juce::Colours::black);
            OutputButton[out].setColour (juce::TextButton::buttonOnColourId, juce::Colours::green);
            OutputButton[out].setInstance (out);
            OutputButton[out].setButtonText (std::string("Out-").append(1,char('A'+out)));
            OutputButton[out].onClick = [this,out] { OutputSet(&OutputButton[out]); };   
            if (busSelected == out) 
                OutputButton[out].setToggleState(true,  juce::dontSendNotification);
            else
                OutputButton[out].setToggleState(false,  juce::dontSendNotification);
        }

        proc->addAudioSwitchListener (this);
    }

    ~Content() 
    {
        proc->removeAudioSwitchListener (this);
    }

    void paint (Graphics& g) override
    {
        g.fillAll (Colors::contentBackgroundColor);
    }

    void resized() override
    {
        auto r = getLocalBounds().reduced (4, 0);

        r.removeFromTop (4);
        r.removeFromRight (20);
        r.removeFromLeft (20);

        busSelectLabel.setBounds (r.removeFromTop (80));

        r.removeFromTop (10);

        auto r2 = r.removeFromTop (30);
        for (int out = 0; out < numOutBuses; out++)         
            OutputButton[out].setBounds (r2.removeFromLeft (240/numOutBuses));
    }

    void updateAudioSwitch (int audioSwitch) override
    {
        busSelected = audioSwitch;
        for (int out = 0; out < numOutBuses; out++)         
            if (busSelected == out) 
                OutputButton[out].setToggleState(true,  juce::dontSendNotification);
            else
                OutputButton[out].setToggleState(false, juce::dontSendNotification);
        repaint();
    }

    void OutputSet(MyTextButton* button) 
    {
        busSelected = button->getInstance();

        //auto currentTime = juce::Time::getCurrentTime();
        //auto includeDate = true;
        //auto includeTime = true;
        //auto currentTimeString = currentTime.toString (includeDate, includeTime);
        //busSelectLabel.setText (currentTimeString, juce::dontSendNotification);

        for (int out = 0; out < numOutBuses; out++)         
            if (busSelected == out)
                OutputButton[out].setToggleState(true,  juce::dontSendNotification);
            else
                OutputButton[out].setToggleState(false, juce::dontSendNotification);
        proc->setSelectedOutputBus(busSelected);
    }
    
private:
    int numOutBuses;
    int busSelected;
    AudioSwitchProcessor* proc;
    Label busSelectLabel;
    MyTextButton OutputButton[8];

};


// ================================================================================================
// AudioSwitchEditor

AudioSwitchEditor::AudioSwitchEditor (AudioSwitchProcessor& _proc)
    : AudioProcessorEditor (&_proc), proc(&_proc)
{
    setOpaque (true);
    content.reset (new Content (*this));
    addAndMakeVisible (content.get());
    setSize (300, 150);    // main window size
}

AudioSwitchEditor::~AudioSwitchEditor()
{
    content.reset();
}

void AudioSwitchEditor::resized()
{
    content->setBounds (getLocalBounds());
}

void AudioSwitchEditor::paint (Graphics& g)
{
    g.fillAll (Colours::grey);
}

} // namespace element
