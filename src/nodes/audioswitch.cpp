/*
    This file is part of Element
    Copyright (C) 2019  Kushview, LLC.  All rights reserved.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#pragma once

#include <element/tags.hpp>
#include "nodes/baseprocessor.hpp"
#include "nodes/audioswitch.h"
#include "nodes/audioswitcheditor.h"
#include "audioswitch.h"

#include <iostream>
//using namespace std;
using std::cout;
using std::endl;

namespace element {

AudioSwitchProcessor::AudioSwitchProcessor()
        : BaseProcessor (BusesProperties()
                                .withInput ("Inp", AudioChannelSet::stereo())
                                .withOutput ("Out-A", AudioChannelSet::stereo())
                                .withOutput ("Out-B", AudioChannelSet::stereo())
                                .withOutput ("Out-C", AudioChannelSet::stereo())
                                .withOutput ("Out-D", AudioChannelSet::stereo()))
{
    //addLegacyParameter (selectedBus = new AudioParameterBool ("selectedBus", "boh!", false));
}

AudioSwitchProcessor::~AudioSwitchProcessor()
{
    selectedBus = nullptr;
}

void AudioSwitchProcessor::prepareToPlay (double sampleRate, int maximumExpectedSamplesPerBlock) 
{
    /** Called before playback starts, to let the processor prepare itself.

        The sample rate is the target sample rate, and will remain constant until
        playback stops.

        You can call getTotalNumInputChannels and getTotalNumOutputChannels
        or query the busLayout member variable to find out the number of
        channels your processBlock callback must process.

        The maximumExpectedSamplesPerBlock value is a strong hint about the maximum
        number of samples that will be provided in each block. You may want to use
        this value to resize internal buffers. You should program defensively in
        case a buggy host exceeds this value. The actual block sizes that the host
        uses may be different each time the callback happens: completely variable
        block sizes can be expected from some hosts.
    */

    //setPlayConfigDetails (numInputs, numOutputs, sampleRate, maximumExpectedSamplesPerBlock);
    setRateAndBufferSizeDetails (sampleRate, maximumExpectedSamplesPerBlock);

    // sync number of output buses
    bool isInput = false;
    cout << "prepareToPlay(): " << endl;
    int currentOutBuses =  getBusCount (isInput); // get output buses actually configured
    cout << ". currentOutputNumBuses = " << currentOutBuses << endl;

    int expectedOutBuses = getOutputBusesNumber(); // the number we wants
    cout << ". expectedOutputNumBuses = " << expectedOutBuses << endl;

    // make the expected available!
    for (; currentOutBuses < expectedOutBuses; currentOutBuses++)
        addBus (isInput);
        
    for (; expectedOutBuses < currentOutBuses; currentOutBuses--)
        removeBus (isInput);

}

//
// If your processor has a total of 2 input channels and 4 output channels, then the buffer will contain 4 channels, 
// the first two being filled with the input data. Your processor should read these, 
// do its processing, and replace the contents of all 4 channels with its output.
//
void AudioSwitchProcessor::processBlock (AudioBuffer<float>& buffer, MidiBuffer&) 
{
    const int numSamples = (int) buffer.getNumSamples();
    const int lastInputChan = getMainBusNumInputChannels();
    const int lastOutputChan = getTotalNumOutputChannels();
    int currentOutBuses =  getBusCount (false); // get output buses actually configured
    int expectedOutBuses = getOutputBusesNumber(); // the number we wants

    //cout << "processBlock(): " << endl;
    //cout << ". currentOutputNumBuses = " << getBusCount(false) << endl;
    //cout << ". expectedOutputNumBuses = " << getOutputBusesNumber() << endl;    
    //for (int ch = 0; ch < lastOutputChan; ch++)
    //    buffer.copyFrom (ch,   0, buffer.getReadPointer (0), numSamples); TBD
    
        //Now copy the input buffer into the currentOutputBus buffers
    //and then zeroes all output buffers not used
    const int chOut = currentOutputBus*2;
    for (int ch = lastInputChan; ch < lastOutputChan; ch=ch+2)
    {
        if (ch==chOut) 
        {
            buffer.copyFrom (ch,   0, buffer.getReadPointer (0), numSamples);
            buffer.copyFrom (ch+1, 0, buffer.getReadPointer (1), numSamples);
        }
        else
        {
            buffer.clear (ch,   0, numSamples);
            buffer.clear (ch+1, 0, numSamples);
        }
    }
    if (chOut!=0)
    {
        //clear buffers 0/1 after having copied it
        buffer.clear(0, 0, numSamples); 
        buffer.clear(1, 0, numSamples); 
    }
    else
    {
        //this is not necessary, as already zeroed, but inject the same amount of fake CPU load also for outA
        buffer.clear(2, 0, numSamples); //clear after having copied it
        buffer.clear(3, 0, numSamples); //clear after having copied it
    }

    //processBlockBypassed (buffer, midiMessages);
}

AudioProcessorEditor* AudioSwitchProcessor::createEditor() 
{
    auto* ed = new AudioSwitchEditor (*this);
    ed->resized();
    return ed;
}

bool AudioSwitchProcessor::isBusesLayoutSupported (const BusesLayout& layout) const
{
    bool retValue;
    // Don't accept to disable the buses
    if (layout.getMainInputChannelSet()  == juce::AudioChannelSet::disabled()
    ||  layout.getMainOutputChannelSet() == juce::AudioChannelSet::disabled())
        return false;
    retValue = layout.getMainInputChannels() == numChanInputs && layout.getMainOutputChannels() == numChanOutputs;
    if (retValue) {
        cout << "isBusesLayoutSupported():" << endl;
        cout << ". getMainInputChannels()  = " << layout.getMainInputChannels()  << ", numChanInputs  = " << numChanInputs  << endl;
        cout << ". getMainOutputChannels() = " << layout.getMainOutputChannels() << ", numChanOutputs = " << numChanOutputs << endl;
        cout << ". retvalue = " << retValue << endl;
    }
    return retValue;

    //return layout.getMainInputChannels() == numChanInputs && layout.getMainOutputChannels() == numChanOutputs;
}

bool AudioSwitchProcessor::canAddBus (bool isInput) const                    
{ 
    if (!isInput) 
    { //only output buses could be added in our case
        if (getOutputBusesNumber() < BusSelectedEnum::outMax)
            return true; 
    }
    return false;
} 

bool AudioSwitchProcessor::canRemoveBus (bool isInput) const                    
{ 
    if (!isInput) 
    { //only output buses could be added in our case
        if (getOutputBusesNumber() > 1)
            return true; 
    }
    return false;
} 

bool AudioSwitchProcessor::canApplyBusCountChange (bool isInput, bool isAdding, AudioProcessor::BusProperties& outProperties)
{
    if (isAdding && ! canAddBus (isInput))
        return false;
    if (! isAdding && ! canRemoveBus (isInput))
        return false;

    auto num = getBusCount (isInput);
    auto* const main = getBus (false, 0);
    if (! main)
        return false;

    if (isAdding)
    {
        outProperties.busName = String (isInput ? "Input #" : "Out-") + char('A'+getBusCount(isInput));
        outProperties.defaultLayout = AudioChannelSet::stereo();
        //outProperties.defaultLayout = (num > 0 ? getBus (isInput, num - 1)->getDefaultLayout()
        //                                    : main->getDefaultLayout());
        outProperties.isActivatedByDefault = true;
    }

    return true;
}


void AudioSwitchProcessor::numBusesChanged(void)
{    
    //numInputs = 0;
    //for (auto i = 0; i < getBusCount(true); ++i)
    //    numInputs += (size_t) getBus (true, i)->getLastEnabledLayout().size();
    //cout << "numInputs = " << numInputs << endl;

    numChanOutputs = 0; //recalculate the number of output channels from scratch
    for (auto i = 0; i < getBusCount(false); ++i)
        numChanOutputs += (size_t) getBus (false, i)->getLastEnabledLayout().size(); //get the size (in channels) for each bus
    cout << "numBusesChanged(): " << endl;
    cout << ". numOutBuses = " << getBusCount(false) << ", ";
    cout << "numOutChannels = " << numChanOutputs << endl;

#if 0

    AudioBuffer<FloatType> = audioBuffers;
    audioBuffers = getBusBuffer (AudioBuffer<FloatType>& processBlockBuffer);


    osChanSize = context.audio.getNumChannels();
    context.audio.setSize()


        @param newNumChannels       the new number of channels.
        @param newNumSamples        the new number of samples.
        @param keepExistingContent  if this is true, it will try to preserve as much of the
                                    old data as it can in the new buffer.
        @param clearExtraSpace      if this is true, then any extra channels or space that is
                                    allocated will be also be cleared. If false, then this space is left
                                    uninitialised.
        @param avoidReallocating    if this is true, then changing the buffer's size won't reduce the
                                    amount of memory that is currently allocated (but it will still
                                    increase it if the new size is bigger than the amount it currently has).
                                    If this is false, then a new allocation will be done so that the buffer
                                    uses takes up the minimum amount of memory that it needs.
    */
    void setSize (int newNumChannels,
                  int newNumSamples,
                  bool keepExistingContent = false,
                  bool clearExtraSpace = false,
                  bool avoidReallocating = false)


#endif
}

void AudioSwitchProcessor::getStateInformation (juce::MemoryBlock& destData)
{
    //read from internal datastore and set into persitency file 
    ValueTree state (tags::state);
    state.setProperty ("numOutputs", numChanOutputs/2, nullptr); //this is the number of buses (all my buses are stereo: 2 channels each!)
    state.setProperty ("outputBus", currentOutputBus, nullptr);
    if (auto xml = state.createXml())
        copyXmlToBinary (*xml, destData);
}

void AudioSwitchProcessor::setStateInformation (const void* data, int size)
{
    //called by restorePluginState(): read from persitency file and set into internal datastore
    ValueTree state;
    if (auto xml = getXmlFromBinary (data, size))
        state = ValueTree::fromXml (*xml);
    if (! state.isValid())
        return;
    numChanOutputs = int(state.getProperty ("numOutputs", 2))*2; //the value stored is the number of buses, so 2 channels each, being fixed stereo
    currentOutputBus = int(state.getProperty ("outputBus", 0));
}

} // namespace element
